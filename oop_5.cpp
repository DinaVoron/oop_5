﻿#include <iostream>
#include <string>
#include <fstream>
#include <vector>
//#include "header.h"


using namespace std;

struct scan_info {

    string model; // наименование модели 
    int price; // цена
    double x_s1ze; // горизонтальный размер области сканирования
    double y__s1ze; // вертикальный размер области сканирования 
    string optr; // оптическое разрешение
    int grey; // число градаций серого
};


bool read_file(ifstream &fi, vector<scan_info> &scin)
{
	int readstr = 0;
	if (fi.is_open())
	{
		int amount;
		string temp = "";
		getline(fi, temp);
		if (temp != "")
		{
			try {
				amount = stoi(temp);
			}
			catch (invalid_argument e) {
				cout << "Amount error" << endl;
				return false;
			}
		}
		else
		{
			cout << "Amount error" << endl;
			return false;
		}
		while (getline(fi, temp) && (readstr < amount))
		{
			readstr = readstr + 1;
			scan_info tempr;
			auto it = temp.begin();
			while (it != temp.end()) //Возможно. это излишне
			{
				while (*it != ' ' && (it != temp.end() - 1)) //считываем модель
				{
					tempr.model = tempr.model + *it;
					it = it + 1;
				}
				if (it == temp.end() - 1)
				{
					cout << "Lack of data in line " << readstr << endl;
					return false;
				}
				else
				{
					it = it + 1;
					string word;
					while (*it != ' ' && (it != temp.end() - 1)) //считываем цену
					{
						word = word + *it;
						it = it + 1;
					}
					if (it == temp.end() - 1)
					{
						cout << "Lack of data in line " << readstr << endl;
						return false;
					}
					else
					{
						if (word != "") //Что произойдет, если будет два пробела подряд?
						{
							tempr.price = stoi(word);
							word = "";
						}
						else
						{
							cout << "Price error in " << readstr << " line" << endl;
							return false;
						}
						it = it + 1;
						while (*it != ' ' && (it != temp.end() - 1)) //считываем горизонтальный размер
						{
							word = word + *it;
							it = it + 1;
						}
						if (it == temp.end() - 1)
						{
							cout << "Lack of data in line " << readstr << endl;
							return false;
						}
						else
						{
							if (word != "")
							{
								tempr.x_s1ze = stoi(word);
								word = "";
							}
							else
							{
								cout << "X size error in " << readstr << " line" << endl;
								return false;
							}
							it = it + 1;
							while (*it != ' ' && (it != temp.end() - 1)) //считываем вертикальный размер
							{
								word = word + *it;
								it = it + 1;
							}
							if (it == temp.end() - 1)
							{
								cout << "Lack of data in line " << readstr << endl;
								return false;
							}
							else
							{
								if (word != "")
								{
									tempr.y__s1ze = stoi(word);
									word = "";
								}
								else
								{
									cout << "Y size error in " << readstr << " line" << endl;
									return false;
								}
								it = it + 1;
								while (*it != ' ' && (it != temp.end() - 1)) 
								{
									tempr.optr = tempr.optr + *it;
									it = it + 1;
								}
								if (it == temp.end() - 1)
								{
									cout << "Lack of data in line " << readstr << endl;
									return false;
								}
								it = it + 1;
								while ((*it != ' ') && (it != temp.end() - 1)) //считываем число градаций серого
								{
									word = word + *it;
									it = it + 1;
								}
								if ((it == temp.end() - 1) && (*it != ' '))
								{
									word = word + *it;
								}
								if (word != "")
								{
									tempr.grey = stoi(word);
									word = "";
								}
								it = temp.end();
							}
						}
					}
				}
			}
			scin.push_back(tempr);
		}
		if (readstr < amount)
		{
			cout << "More recordings were planned" << endl;
			return false;
		}
	}
	else
	{
		cout << "File was not opened";
		return false;
	}
}

void sort_price(vector<scan_info> &scin)
{
	int i, j;
	int n = scin.size();
	for (j = 1; j < n; j++)
	{
		for (i = 0; i < n - j; i++)
		{
			if (scin[i].price > scin[i + 1].price)
			{
				scan_info t;
				t = scin[i];
				scin[i] = scin[i + 1];
				scin[i + 1] = t;
			}
		}	
	}
}

void sort_grey(vector<scan_info>& scin)
{
	int i, j;
	int n = scin.size();
	for (j = 1; j < n - 1; j++)
	{
		for (i = 0; i < n - 1 - j; i++)
		{
			if (scin[i].grey > scin[i + 1].grey)
			{
				scan_info t;
				t = scin[i];
				scin[i] = scin[i + 1];
				scin[i + 1] = t;
			}
		}
	}
}

void print_array(vector<scan_info> scin)
{
	for (int i = 0; i < scin.size(); i++)
	{
		cout << scin[i].model << " " << scin[i].price << " " << scin[i].x_s1ze << " " << scin[i].y__s1ze << " " << scin[i].optr << " " << scin[i].grey << endl;
	}
	cout << endl;
}


void fill_fout(vector<scan_info> scin, ofstream &fout)
{
	int sz = scin.size();
	fout.write(reinterpret_cast<char*>(&sz), sizeof(sz));
	fout.seekp(4, ios_base::beg);
	for (int i = 0; i < scin.size(); i++)
	{
		fout.write(reinterpret_cast<char*>(&scin[i].model), sizeof(scin[i].model));
		fout.write(reinterpret_cast<char*>(&scin[i].x_s1ze), sizeof(scin[i].x_s1ze));
		fout.write(reinterpret_cast<char*>(&scin[i].y__s1ze), sizeof(scin[i].y__s1ze));
		fout.write(reinterpret_cast<char*>(&scin[i].optr), sizeof(scin[i].optr));
		fout.write(reinterpret_cast<char*>(&scin[i].grey), sizeof(scin[i].grey));
	}
}


int main()
{
	vector<scan_info> scin; //массив записей про сканеры
	char sort;
	ifstream fi("input2.txt");
	ofstream fout("output.bin", ios::out | ios::binary);
	if (read_file(fi,scin))
	{
		cout << "If you want to sort by price than enter 'p'. Else enter 'g'" << endl;
		cin >> sort;
		while (std::cin.fail() || (sort != 'p' && sort != 'g'))
		{
			cin.clear();
			cin.ignore();
			cout << "Inter 'g' or 'p' again" << endl;
			cin >> sort;
		}
		if (sort == 'p')
		{
			sort_price(scin);
			print_array(scin);
		}
		if (sort == 'g')
		{
			sort_grey(scin);
			print_array(scin);
		}
		fill_fout(scin,fout);
	}
	fi.close(); 
	fout.close();
	return 0;
}